CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
PROJECT(LEVMAR)

option(HAVE_LAPACK "Do we have lapack" ON)
option(HAVE_PLASMA "Do we have PLASMA? (http://icl.cs.utk.edu/plasma/)" OFF)
option(LINSOLVERS_RETAIN_MEMORY "Should linear solvers retain working memory between calls? (non-reentrant!)" ON)

option(LM_DBL_PREC "Build double precision routines?" ON)
option(LM_SNGL_PREC "Build single precision routines?" ON)

option(BUILD_DEMO "Build demo program?" TRUE)

list(APPEND SRC lm.c Axb.c misc.c lmlec.c lmbc.c lmblec.c lmbleic.c)
list(APPEND LIBS m)

if(HAVE_LAPACK)
	option(BLA_STATIC "use static lapack libraries" OFF)
	find_package(LAPACK REQUIRED)
	list(APPEND LIBS ${LAPACK_LIBRARIES})
endif(HAVE_LAPACK)

if(HAVE_PLASMA)
	SET(PLASMA_DIR "/usr/local/PLASMA" CACHE PATH "Path to PLASMA root")
	SET(PLASMA_LIB_NAMES "plasma;coreblas;quark;lapacke" CACHE STRING "The names of the PLASMA libraries")
	list(append LIBS ${PLASMA_LIB_NAMES})
endif(HAVE_PLASMA)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/levmar.h.in ${CMAKE_CURRENT_SOURCE_DIR}/levmar.h)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/levmar.h DESTINATION include COMPONENT Development)

# compiler flags
#ADD_DEFINITIONS(-DLINSOLVERS_RETAIN_MEMORY) # do not free memory between linear solvers calls
#REMOVE_DEFINITIONS(-DLINSOLVERS_RETAIN_MEMORY) # free memory between calls

############################################################
# macros to generate libs
############################################################
macro( build_lib name type sources deps soversion version)
	message(STATUS "${name} will be built as ${type} and installed to ${CMAKE_INSTALL_PREFIX}/lib")

	#build the library as -shared or as -static
	add_library( ${name}-${type} ${type} ${sources} )
	target_link_libraries( ${name}-${type} ${deps})
	set_target_properties( ${name}-${type} PROPERTIES SOVERSION ${soversion} VERSION ${version} OUTPUT_NAME ${name} INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")

	# install libraries
	install (TARGETS ${name}-${type}
		RUNTIME DESTINATION bin COMPONENT RuntimeLibraries
		LIBRARY DESTINATION lib COMPONENT RuntimeLibraries
		ARCHIVE DESTINATION lib COMPONENT Development
	)

	set(${name}_lib ${name}-${type} CACHE INTERNAL "internal name for ${name}" FORCE)
endmacro( build_lib )

############################################################
# actual build
############################################################

include_directories(${CMAKE_SOURCE_DIR})
if(HAVE_PLASMA)
	include_directories(${PLASMA_DIR}/include ${PLASMA_DIR}/quark)
	# PLASMA headers in Axb.c should be compiled as C++
	set_source_files_properties(Axb.c PROPERTIES LANGUAGE CXX)
endif(HAVE_PLASMA)

message(STATUS "linking against ${LIBS}")

build_lib(levmar SHARED "${SRC}" "${LIBS}" 2 2.6)
build_lib(levmar STATIC "${SRC}" "${LIBS}" 2 2.6)

# demo program
if(BUILD_DEMO)
	add_executable(lmdemo lmdemo.c levmar.h)
	target_link_libraries(lmdemo levmar-SHARED )
	install(TARGETS lmdemo RUNTIME DESTINATION bin COMPONENT "demo")
endif(BUILD_DEMO)

#SUBDIRS(matlab)
